---
layout: blog
title: nextblog
date: 2022-01-11T11:15:21.090Z
---
When I was building my own [website](https://telmo.im/) and blog, I chose to use Markdown in conjunction with Next.js because it’s easy, fast, and, to be honest, I enjoy writing in Markdown. In this article, I’ll walk through how I did this so you can produce a great, content-driven site of your own.