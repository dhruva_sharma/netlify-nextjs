---
layout: blog
title: testblog
date: 2022-01-11T12:27:24.316Z
---
# Setting up the project

First, create a folder for your project. Do this by running:

```bash

```

For those new to UNIX commands, this produces a new directory named ‘blog’ (`mkdir blog`) and navigates to it (`cd blog`).