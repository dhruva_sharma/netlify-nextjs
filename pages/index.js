import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
// import Head from 'next/head'
// import Layout, { siteTitle } from '../components/layout'
// import utilStyles from '../styles/utils.module.css'

function Home(props) {
  return (
        <h1>Welcome to the blog: {props.blogTitle}!</h1>
  )
}

Home.getInitialProps = () => {
  return {
    blogTitle: ' Learning for life!'
  }
}

export default Home