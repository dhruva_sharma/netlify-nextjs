import '../styles/global.css'

// import styles from './alert.module.css'
// import cn from 'classnames'

export default function App({ Component, pageProps }) {
  return <Component {...pageProps} />
}

// export default function Alert({ children, type }) {
//   return (
//     <div
//       className={cn({
//         [styles.success]: type === 'success',
//         [styles.error]: type === 'error'
//       })}
//     >
//       {children}
//     </div>
//   )
// }